#include <goocanvas.h>
#include <libsoup/soup.h>
#include <json-glib/json-glib.h>

GtkBuilder *builder;
GtkWidget *window, *canvas;
GooCanvasItem *root;
GdkPixbuf *map_img;
GooCanvasItem *map;
GtkWidget *map_scroll;
GtkWidget *serverlist;
GtkWidget *select_all, *select_none, *apply;
SoupSession *session;
GFile *blocked_file;
GRegex *safety_regex;

#define WIDTH 3840.0
#define HEIGHT 1920.0

#define ALLOWED_COLOR "lime"
#define BLOCKED_COLOR "red"
#define SELECTED_COLOR "cyan"

typedef struct {
    const char *symbol, *desc;
    const char *relays;
    GooCanvasItem *text;
    GtkWidget *list_item, *checkbox;
} Server;

GList *servers = NULL;

void copy_to_clipboard(char *text){
    GtkClipboard *clipboard = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
    gtk_clipboard_set_text(clipboard, text, -1);
}

void calc_x_y(double *x, double *y, double latitude, double longtitude, double width, double height){
    *x = width / 2.0 + width * longtitude / 360.0;
    *y = height / 2.0 - height * latitude / 180.0; 
}

void toggled(GtkToggleButton *toggle_button, gpointer user_data){
    Server *server = user_data;
    if(gtk_toggle_button_get_active(toggle_button)){
        g_object_set(server->text, "fill-color", BLOCKED_COLOR, NULL);
    }else{
        g_object_set(server->text, "fill-color", ALLOWED_COLOR, NULL);
    }
}

gboolean text_pressed(GooCanvasItem *item, GooCanvasItem *target_item, GdkEvent *event, gpointer user_data){
    GdkEventButton *evt = (GdkEventButton*)event;
    if(evt->button != 1) return FALSE;

    Server *srv = user_data;
    
    GtkListBoxRow *row = GTK_LIST_BOX_ROW(gtk_widget_get_parent(srv->list_item));
    gtk_list_box_select_row(GTK_LIST_BOX(serverlist), row);

    GtkAdjustment *adj = gtk_list_box_get_adjustment(GTK_LIST_BOX(serverlist));

    gint y;
    gtk_widget_translate_coordinates(GTK_WIDGET(row), serverlist, 0, 0, NULL, &y);
    gtk_adjustment_set_value(adj, y);

    return FALSE;
}

void handle_server(const char *symbol, const char *desc, double longtitude, double latitude, char **relays){
    const char *ips = g_strjoinv(",", relays);

    double x, y;
    calc_x_y(&x, &y, latitude, longtitude, WIDTH, HEIGHT);

    Server *server = g_new0(Server, 1);

    server->text = goo_canvas_text_new(root, symbol, x, y, -1, GOO_CANVAS_ANCHOR_CENTER, "fill-color", ALLOWED_COLOR, "font", "monospace", NULL);
    g_signal_connect(server->text, "button-press-event", G_CALLBACK(text_pressed), server);

    server->relays = ips;
    server->symbol = g_strdup(symbol);
    server->desc = g_strdup(desc);
    
    server->list_item = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 4);
    server->checkbox = gtk_check_button_new();
    g_signal_connect(server->checkbox, "toggled", G_CALLBACK(toggled), server);
    gtk_container_add(GTK_CONTAINER(server->list_item), server->checkbox);
    GtkWidget *list_item_label_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_widget_set_hexpand(list_item_label_box, TRUE);
    gtk_container_add(GTK_CONTAINER(server->list_item), list_item_label_box);
    GtkWidget *symbol_label = gtk_label_new(symbol);
    gtk_widget_set_halign(symbol_label, GTK_ALIGN_START);
    gtk_widget_set_opacity(symbol_label, 0.5);
    gtk_container_add(GTK_CONTAINER(list_item_label_box), symbol_label);
    GtkWidget *desc_label = gtk_label_new(desc);
    gtk_widget_set_halign(desc_label, GTK_ALIGN_START);
    gtk_container_add(GTK_CONTAINER(list_item_label_box), desc_label);

    gtk_widget_show_all(server->list_item);
    gtk_list_box_insert(GTK_LIST_BOX(serverlist), server->list_item, -1);

    servers = g_list_append(servers, server);
}

void row_selected(GtkListBox *box, GtkListBoxRow *row, gpointer user_data){
    if(row == NULL) return;
    GtkWidget *child = gtk_bin_get_child(GTK_BIN(row));
    GList *server = servers;
    while(server != NULL){
        Server *srv = server->data;
        if(srv->list_item == child){
            g_object_set(srv->text, "fill-color", SELECTED_COLOR, NULL);
        }else{
            if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(srv->checkbox))){
                g_object_set(srv->text, "fill-color", BLOCKED_COLOR, NULL);
            }else{
                g_object_set(srv->text, "fill-color", ALLOWED_COLOR, NULL);
            }
        }
        server = server->next;
    }
}

void response_callback(SoupSession *session, SoupMessage *msg, gpointer user_data){
    SoupMessageBody *body = msg->response_body;

    GError *error = NULL;
    JsonNode *response_node = json_from_string(body->data, &error);
    if(!response_node){
        g_print("Error getting server list: %s\n", error->message);
        g_error_free(error);
        g_free(user_data);
        return;
    }

    JsonObject *response_obj = json_node_get_object(response_node);
    JsonObject *pops = json_object_get_object_member(response_obj, "pops");
    GList *pops_keys = json_object_get_members(pops);
    GList *key = pops_keys;
    while(key != NULL){

        JsonObject *pop = json_object_get_object_member(pops, key->data);

        if(!json_object_has_member(pop, "relays")){
            key = key->next;
            continue;
        }

        const char *desc = json_object_get_string_member(pop, "desc");

        JsonArray *geo = json_object_get_array_member(pop, "geo");
        double longtitude = json_array_get_double_element(geo, 0);
        double latitude = json_array_get_double_element(geo, 1);

        JsonArray *relays = json_object_get_array_member(pop, "relays");
        guint n_relays = json_array_get_length(relays);
        char *relay_ips[n_relays + 1];
        relay_ips[n_relays] = NULL;
        for(guint i = 0; i < n_relays; i++){
            JsonObject *relay = json_array_get_object_element(relays, i);
            relay_ips[i] = (char*)json_object_get_string_member(relay, "ipv4");
        }

        handle_server(key->data, desc, longtitude, latitude, relay_ips);

        key = key->next;
    }
    g_list_free(pops_keys);

    json_node_free(response_node);
}

void select_all_items(GtkButton *button, gpointer user_data){
    GList *server = servers;
    while(server != NULL){
        Server *srv = server->data;
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(srv->checkbox), TRUE);
        server = server->next;
    }
}

void deselect_all_items(GtkButton *button, gpointer user_data){
    GList *server = servers;
    while(server != NULL){
        Server *srv = server->data;
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(srv->checkbox), FALSE);
        server = server->next;
    }
}

// I hate async in C

// typedef struct {
//     GFileInputStream *str;
//     char *ips;
// } ApplyFirewall;

// void blocked_file_size_cb(GObject *source_object, GAsyncResult *res, gpointer data){
//     GFileInfo *info = g_file_input_stream_query_info_finish(source_object, res, NULL);
//     if(!info){
//         return;
//     }
//     guint64 size = g_file_info_get_attribute_uint64(info, G_FILE_ATTRIBUTE_STANDARD_SIZE);

// }

// void open_blocked_file_cb(GObject *source_object, GAsyncResult *res, gpointer data){
//     GFileInputStream *str = g_file_read_finish(G_FILE(source_object), res, NULL);
//     if(!str){
//         return;
//     }
//     ApplyFirewall *afw = g_new0(ApplyFirewall);
//     afw->ips = data;
//     afw->str = 
//     g_file_input_stream_query_info_async(str, G_FILE_ATTRIBUTE_STANDARD_SIZE, G_PRIORITY_DEFAULT, NULL, blocked_file_size_cb, data);
// }

void unblock_and_block(char *old_blocked, char *new_blocked){
    if(old_blocked != NULL) if(!g_regex_match(safety_regex, old_blocked, G_REGEX_MATCH_DEFAULT, NULL)) old_blocked = NULL;
    if(new_blocked != NULL) if(!g_regex_match(safety_regex, new_blocked, G_REGEX_MATCH_DEFAULT, NULL)) new_blocked = NULL;

    if(old_blocked == NULL && new_blocked == NULL) return;
    char *command;
    if(old_blocked == NULL && new_blocked != NULL){
        command = g_strdup_printf("pkexec sh -c 'iptables -A INPUT -s %s -j DROP'", new_blocked);
    }else if(old_blocked != NULL && new_blocked == NULL){
        command = g_strdup_printf("pkexec sh -c 'iptables -D INPUT -s %s -j DROP'", old_blocked);
    }else{
        command = g_strdup_printf("pkexec sh -c 'iptables -D INPUT -s %s -j DROP; iptables -A INPUT -s %s -j DROP'", old_blocked, new_blocked);
    }
    g_print("%s\n", command);
    g_spawn_command_line_sync(command, NULL, NULL, NULL, NULL);
}

// Fuck it, block the entire thread
void apply_firewall(char *joined_ips){
    // char *command = g_strdup_printf("sudo iptables -A INPUT -s %s -j DROP", joined_ips);
    // g_free(command);

    // copy_to_clipboard(command);

    // g_file_read_async(blocked_file, G_PRIORITY_DEFAULT, NULL, open_blocked_file_cb, joined_ips);

    char *old_blocked = NULL;
    GFileInputStream *str = g_file_read(blocked_file, NULL, NULL);
    if(str){
        GFileInfo *info = g_file_input_stream_query_info(str, G_FILE_ATTRIBUTE_STANDARD_SIZE, NULL, NULL);
        guint64 size = g_file_info_get_attribute_uint64(info, G_FILE_ATTRIBUTE_STANDARD_SIZE);
        old_blocked = g_malloc0(size + 1);
        g_input_stream_read(G_INPUT_STREAM(str), old_blocked, size, NULL, NULL);
        g_input_stream_close(G_INPUT_STREAM(str), NULL, NULL);
        if(size == 0){
            g_free(old_blocked);
            old_blocked = NULL;
        }
    }
    
    GFileOutputStream *ostr = g_file_replace(blocked_file, NULL, FALSE, G_FILE_CREATE_PRIVATE, NULL, NULL);
    if(joined_ips != NULL) g_output_stream_write(G_OUTPUT_STREAM(ostr), joined_ips, strlen(joined_ips), NULL, NULL);
    g_output_stream_close(G_OUTPUT_STREAM(ostr), NULL, NULL);

    unblock_and_block(old_blocked , joined_ips);

    g_free(joined_ips);
    g_free(old_blocked);

    gtk_widget_set_sensitive(GTK_WIDGET(apply), TRUE);
}

void apply_blocklist(GtkButton *button, gpointer user_data){
    GList *blocked = NULL;
    gint num_blocked = 0;
    GList *server = servers;
    while(server != NULL){
        Server *srv = server->data;
        if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(srv->checkbox))){
            blocked = g_list_append(blocked, srv);
            num_blocked++;
        }
        server = server->next;
    }
    
    char *relay_ips[num_blocked + 1];
    relay_ips[num_blocked] = NULL;

    gint i = 0;
    server = blocked;
    while(server != NULL){
        Server *srv = server->data;
        relay_ips[i] = (char*)srv->relays;
        i++;
        server = server->next;
    }
    g_list_free(blocked);

    char *joined_ips = g_strjoinv(",", relay_ips);
    if(strlen(joined_ips) == 0){
        g_free(joined_ips);
        joined_ips = NULL;
    }

    gtk_widget_set_sensitive(GTK_WIDGET(button), FALSE);

    apply_firewall(joined_ips);
}

guint canvas_button = 0;
double canvas_press_x, canvas_press_y;
GtkAdjustment *scroll_adj_h, *scroll_adj_v;

gboolean mouse_move(GtkWidget *widget, GdkEvent *event, gpointer user_data){
    GdkEventMotion *evt = (GdkEventMotion*)event;
    if(canvas_button > 0){
        double x = gtk_adjustment_get_value(scroll_adj_h);
        x -= (evt->x - canvas_press_x);
        gtk_adjustment_set_value(scroll_adj_h, x);
        double y = gtk_adjustment_get_value(scroll_adj_v);
        y -= (evt->y - canvas_press_y);
        gtk_adjustment_set_value(scroll_adj_v, y);
    }
    return FALSE;
}

gboolean button_press(GtkWidget *widget, GdkEvent *event, gpointer user_data){
    GdkEventButton *evt = (GdkEventButton*)event;
    if(evt->button == 2 || evt->button == 3){
        canvas_button = evt->button;
        canvas_press_x = evt->x;
        canvas_press_y = evt->y;
    }
    return FALSE;
}

gboolean button_release(GtkWidget *widget, GdkEvent *event, gpointer user_data){
    GdkEventButton *evt = (GdkEventButton*)event;
    if(evt->button == canvas_button) canvas_button = 0;
    return FALSE;
}

int main(int argc, char *argv[]){
    gtk_init(&argc, &argv);

    safety_regex = g_regex_new("^[,.0-9]*$", G_REGEX_DEFAULT, G_REGEX_MATCH_DEFAULT, NULL);

    char *config_dir = g_build_filename(g_get_home_dir(), ".config", "csgo-iptables", NULL);
    g_mkdir_with_parents(config_dir, 0700);
    blocked_file = g_file_new_build_filename(config_dir, "blocked", NULL);

    builder = gtk_builder_new_from_resource("/ui.glade");

    window = GTK_WIDGET(gtk_builder_get_object(builder, "window"));
    serverlist = GTK_WIDGET(gtk_builder_get_object(builder, "serverlist"));
    map_scroll = GTK_WIDGET(gtk_builder_get_object(builder, "map_scroll"));
    select_all = GTK_WIDGET(gtk_builder_get_object(builder, "select_all"));
    select_none = GTK_WIDGET(gtk_builder_get_object(builder, "select_none"));
    apply = GTK_WIDGET(gtk_builder_get_object(builder, "apply"));

    g_signal_connect(select_all, "clicked", G_CALLBACK(select_all_items), NULL);
    g_signal_connect(select_none, "clicked", G_CALLBACK(deselect_all_items), NULL);
    g_signal_connect(apply, "clicked", G_CALLBACK(apply_blocklist), NULL);
    g_signal_connect(serverlist, "row-selected", G_CALLBACK(row_selected), NULL);

    scroll_adj_h = gtk_scrolled_window_get_hadjustment(GTK_SCROLLED_WINDOW(map_scroll));
    scroll_adj_v = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(map_scroll));

    g_signal_connect(map_scroll, "motion-notify-event", G_CALLBACK(mouse_move), NULL);
    g_signal_connect(map_scroll, "button-press-event", G_CALLBACK(button_press), NULL);
    g_signal_connect(map_scroll, "button-release-event", G_CALLBACK(button_release), NULL);

    canvas = goo_canvas_new();

    // goo_canvas_set_scale(GOO_CANVAS(canvas), 1.0 / 4.0);
    goo_canvas_set_bounds(GOO_CANVAS(canvas), 0, 0, WIDTH, HEIGHT);
    // gtk_widget_set_size_request(canvas, WIDTH, HEIGHT);

    root = goo_canvas_get_root_item(GOO_CANVAS(canvas));

    map_img = gdk_pixbuf_new_from_resource("/map.png", NULL);

    map = goo_canvas_image_new(GOO_CANVAS_ITEM(root), map_img, 0, 0, NULL);

    gtk_container_add(GTK_CONTAINER(map_scroll), canvas);

    gtk_widget_show_all(window);

    session = soup_session_new();
    SoupMessage *servers_msg = soup_message_new("GET", "https://api.steampowered.com/ISteamApps/GetSDRConfig/v1?appid=730");
    soup_session_queue_message(session, servers_msg, response_callback, NULL);

    g_signal_connect(window, "delete-event", gtk_main_quit, NULL);

    gtk_main();
}