# CS:GO iptables
GTK3 application for GNU/Linux that lets you block selected CS:GO relays with iptables.

![screenshot](screenshot.png)

## Use case
The only obvious use case (to me) is to avoid playing on Russian-infested servers, because (most) Russian players:
- Refuse to speak English
- Mic spam constantly
- Swear excessively (nahui, blyat, pizda, etc.)
- Don't function in a team
- Are often just bad at the game

## Usage
Because CS:GO uses Steam Datagram Relay, it's not possible to block gameservers themselves. The most effective way to prevent playing on particular game servers is to use this application in combination with the maximum acceptable ping setting in the game.

For example, if you are in the Netherlands, you can block Amsterdam, Warsaw, Stockholm (1 and 2) and Frankfurt, and set the maximum ping to 30 or lower. This will force the game to use relays in London and Paris (maybe also Strasbourg and Luxembourg City), making it unlikely (though not 100% impossible) to connect to Stockholm and Warsaw servers (where many Russians play).

## Features
- Map with relay locations
- Allows choosing every relay to block
- Pulls up-to-date relay list on startup
- Saves blocked relays to remove the right iptables rules when unblocking
- Use pkexec to update iptables

## Lacking features
- Rules are not persistent between reboots
- Zoomable or more user-friendly map
- Packaging or installation

## Building
You will need gcc, make, and these libraries (or your distro's equivalent):
- libgoocanvas-2.0-dev
- libjson-glib-dev
- libsoup2.4-dev

Just run `make`. You can run it with `make run`.

## To do
- Asynchronous file I/O
- Asynchronous iptables update
- Zoomable map
- Do something about overlapping relay locations
- Installation/packaging
