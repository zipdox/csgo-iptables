CC=gcc
FLAGS=`pkg-config --cflags goocanvas-2.0 libsoup-2.4 json-glib-1.0`
LIBS=`pkg-config --libs goocanvas-2.0 libsoup-2.4 json-glib-1.0`
PREFIX=/usr
BUILD_DIR=build
SRCS = $(shell find ./src/*.c)
OBJS = $(patsubst %.c, $(BUILD_DIR)/%.o, $(SRCS))
OPTIMIZATION=-O3
BINARY=csgo-iptables

all: $(BUILD_DIR)/$(BINARY)

$(BUILD_DIR)/assets/: assets/
	mkdir -p $(dir $@)
	cp -r assets/* $(dir $@)
$(BUILD_DIR)/assets/gschemas.compiled: $(BUILD_DIR)/assets/
	glib-compile-schemas $(dir $@)
$(BUILD_DIR)/resources.c: resources.xml $(BUILD_DIR)/assets/gschemas.compiled
	glib-compile-resources --sourcedir=$(BUILD_DIR)/assets/ $< --target=$@ --generate-source
$(BUILD_DIR)/resources.o: $(BUILD_DIR)/resources.c
	$(CC) -c -o $@ $^ $(FLAGS) $(OPTIMIZATION)

$(BUILD_DIR)/%.o: %.c
	mkdir -p $(dir $@)
	$(CC) -c -o $@ $(FLAGS) $< -Wall $(OPTIMIZATION)

$(BUILD_DIR)/$(BINARY): $(OBJS) $(BUILD_DIR)/resources.o
	$(CC) -o $@ $^ $(LIBS) $(OPTIMIZATION)

clean:
	rm -rf build

run:
	./build/$(BINARY)
